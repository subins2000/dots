package com.subinsb.dots;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ServiceInfo;
import android.content.res.AssetManager;
import android.net.Uri;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.subinsb.wtt.WebTorrentTracker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URL;

import fi.iki.elonen.SimpleWebServer;

public class MainActivity extends AppCompatActivity implements ViewTreeObserver.OnScrollChangedListener {

    static String httpServerHostname = "0.0.0.0";
    static int httpServerPort = 8086;
    static int trackerPort = 8085;

    static String SERVICE_NAME = "WebTorrentTracker";
    static String SERVICE_TYPE = "_wtt._tcp.";

    protected WebView webview;
    protected SwipeRefreshLayout swipeLayout;
    private String myServiceName = SERVICE_NAME;

    private ArrayList<String> trackerURLs = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webview = (WebView) findViewById(R.id.webview);

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setDomStorageEnabled(true);

        webview.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                updateProgress(progress);
            }
        });

        webview.setWebViewClient(new WebViewClient(){
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                boolean override = false;
                try {
                    URL u = new URL(url);
                    if (u.getHost().equals(httpServerHostname)) {
                        view.loadUrl(url);
                    } else {
                        Context context = view.getContext();
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        context.startActivity(browserIntent);
                        override = true;
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                return override;
            }

            public void onPermissionRequest(final PermissionRequest request) {
                Log.d("dots", "onPermissionRequest");
                runOnUiThread(new Runnable() {
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void run() {
                        if(request.getOrigin().toString().equals("http://" + httpServerHostname)) {
                            request.grant(request.getResources());
                        } else {
                            request.deny();
                        }
                    }
                });
            }
        });

        if (Build.VERSION.SDK_INT >= 21) {
            webview.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // chromium, enable hardware acceleration
            webview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // older android version, disable hardware acceleration
            webview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        webview.setVisibility(View.VISIBLE);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        swipeLayout.setEnabled(true);

        webview.getViewTreeObserver().addOnScrollChangedListener(this);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                webview.reload();
                swipeLayout.setRefreshing(false);

                new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            insertTrackers();
                        }
                    },
                    1000
                );
            }
        });

        init();
    }

    @Override
    public void onScrollChanged() {
        if(webview.getScrollY() > 0)
            swipeLayout.setEnabled(false);
        else
            swipeLayout.setEnabled(true);
    }

    private void init() {
        startTracker();

        String appDataFolder = getFilesDir().getPath();

        if (getInstallStatus() < 2) {
            firstRunInstall(appDataFolder);
        }

        startServer(appDataFolder + "/vett");

        webview.loadUrl("http://" + httpServerHostname + ":" + httpServerPort + "/");
        //webview.loadUrl("http://192.168.43.64:8080/");

        // Discover others !
        discover();
    }

    // Returns
    // 0: The APP is First Install
    // 1: The APP is updated
    // 2: The APP has no install change
    // Modified version
    // Thanks Webserveis https://stackoverflow.com/a/43570229/1372424
    private int getInstallStatus() {
        SharedPreferences sp = getSharedPreferences("MYAPP", 0);
        int result, currentVersionCode = BuildConfig.VERSION_CODE;
        int lastVersionCode = sp.getInt("FIRSTTIMERUN", -1);
        if (lastVersionCode == -1)
            result = 0;
        else
            result = (lastVersionCode == currentVersionCode) ? 2 : 1;
        sp.edit().putInt("FIRSTTIMERUN", currentVersionCode).apply();
        return result;
    }

    private void firstRunInstall(String appDataFolder) {
        // Install

        String abi;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            abi = Build.SUPPORTED_ABIS[0];
        } else {
            abi = Build.CPU_ABI;
        }

        copyAsset("vett.zip",  appDataFolder + "/vett.zip");

        File zf = new File(appDataFolder + "/vett.zip");

        try {
            Utils.unzip(zf, new File(appDataFolder));
        } catch (Exception e) {
            Log.e("dots", e.toString());
        }
    }

    private static void startTracker() {
        Runnable myRunnable = new Runnable() {
            @Override
            public void run()  {
                try {
                    WebTorrentTracker wt = new WebTorrentTracker(new InetSocketAddress("0.0.0.0", trackerPort));
                    wt.start();
                } catch (Exception e) {
                    Log.e("dots", e.toString());
                    e.printStackTrace();
                }
            }
        };
        new Thread(myRunnable).start();
    }

    private static void startServer(String rootDir) {
        final String _rootDir = rootDir;
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                runServer(_rootDir);
            }
        };
        new Thread(myRunnable).start();
    }

    private static void runServer(String rootDir) {
        try {
            new SimpleWebServer("0.0.0.0", httpServerPort, new File(rootDir), true).start();
        } catch (Exception e) {
            Log.e("dots", e.toString());
        }
    }

    public void updateProgress(int progress) {
        final ProgressBar Pbar = (ProgressBar) findViewById(R.id.browserProgress);
        if (progress < 100 && Pbar.getVisibility() == ProgressBar.GONE) {
            Pbar.setVisibility(ProgressBar.VISIBLE);
        }

        Pbar.setProgress(progress);
        if (progress == 100) {
            Pbar.setVisibility(ProgressBar.GONE);
        }
    }

    // Add a tracker to 
    public void addTracker(url) {
        trackerURLs.add(url);
    }

    // Call addTracker in JavaScript
    public void insertTrackers() {
        // TODO: p2pt may not be available all the time
        String script = 
            "function docReady(fn) {" +
            "    // see if DOM is already available" +
            "    if (document.readyState === 'complete' || document.readyState === 'interactive') {" +
            "        // call on next available tick" +
            "        setTimeout(fn, 1);" +
            "    } else {" +
            "        document.addEventListener('DOMContentLoaded', fn);" +
            "    }" +
            "}";

        for (int i = 0; i < trackerURLs.size(); i++) {
            script += "docReady(function () { window.$ADD_TRACKER('" + trackerURLs.get(i) + "'); })";
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            webview.evaluateJavascript(script, null);
        } else {
            webview.loadUrl("javascript:" + script + ";");
        }
    }

    private void discover() {
        // Using NSD : https://developer.android.com/training/connect-devices-wirelessly/nsd
        registerTrackerService();
        serviceListener();
    }

    private class ResolveListener implements NsdManager.ResolveListener {

        @Override
        public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
            // Called when the resolve fails. Use the error code to debug.
            Log.e("dots", "Resolve failed: " + errorCode);
        }

        @Override
        public void onServiceResolved(NsdServiceInfo serviceInfo) {
            Log.e("dots", "Resolve Succeeded. " + serviceInfo);

            if (serviceInfo.getServiceName().equals(SERVICE_NAME)) {
                Log.d("dots", "Same IP.");
                return;
            }
            final int port = serviceInfo.getPort();
            final InetAddress host = serviceInfo.getHost();

            Log.d("dots", "Found device at " + host + ":" + port);

            webview.post(new Runnable() {
                @Override
                public void run() {
                    addTracker("ws://" + host + ":" + port);
                    insertTrackers();
                }
            });
        }
    };

    private void serviceListener() {
        // ----
        // Discover trackers in network
        // ----

        final NsdManager nsdManager = (NsdManager) getApplicationContext().getSystemService(Context.NSD_SERVICE);

        // Instantiate a new DiscoveryListener
        NsdManager.DiscoveryListener discoveryListener = new NsdManager.DiscoveryListener() {

            // Called as soon as service discovery begins.
            @Override
            public void onDiscoveryStarted(String regType) {
                Log.d("dots", "Service discovery started");
            }

            @Override
            public void onServiceFound(NsdServiceInfo service) {
                // A service was found! Do something with it.
                Log.d("dots", "Service discovery success" + service);
                if (!service.getServiceType().equals(SERVICE_TYPE)) {
                    // Service type is the string containing the protocol and
                    // transport layer for this service.
                    Log.d("dots", "Unknown Service Type: " + service.getServiceType());
                } else if (service.getServiceName().equals(SERVICE_NAME)) {
                    // The name of the service tells the user what they'd be
                    // connecting to. It could be "Bob's Chat App".
                    Log.d("dots", "Same machine: " + SERVICE_NAME);
                } else if (service.getServiceName().contains(SERVICE_NAME)){
                    nsdManager.resolveService(service, new ResolveListener());
                }
            }

            @Override
            public void onServiceLost(NsdServiceInfo service) {
                // When the network service is no longer available.
                // Internal bookkeeping code goes here.
                Log.e("dots", "service lost: " + service);
            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                Log.i("dots", "Discovery stopped: " + serviceType);
            }

            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                Log.e("dots", "Discovery failed: Error code:" + errorCode);
                nsdManager.stopServiceDiscovery(this);
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                Log.e("dots", "Discovery failed: Error code:" + errorCode);
                nsdManager.stopServiceDiscovery(this);
            }
        };

        nsdManager.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, discoveryListener);
    }

    private void registerTrackerService() {
        // Register our tracker service

        NsdManager.RegistrationListener registrationListener = new NsdManager.RegistrationListener() {
            @Override
            public void onServiceRegistered(NsdServiceInfo NsdServiceInfo) {
                // Save the service name. Android may have changed it in order to
                // resolve a conflict, so update the name you initially requested
                // with the name Android actually used.
                myServiceName = NsdServiceInfo.getServiceName();
            }

            @Override
            public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
                // Registration failed! Put debugging code here to determine why.
            }

            @Override
            public void onServiceUnregistered(NsdServiceInfo arg0) {
                // Service has been unregistered. This only happens when you call
                // NsdManager.unregisterService() and pass in this listener.
            }

            @Override
            public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
                // Unregistration failed. Put debugging code here to determine why.
            }
        };

        // Create the NsdServiceInfo object, and populate it.
        NsdServiceInfo serviceInfo = new NsdServiceInfo();

        // The name is subject to change based on conflicts
        // with other services advertised on the same network.
        serviceInfo.setServiceName(SERVICE_NAME);
        serviceInfo.setServiceType(SERVICE_TYPE);
        serviceInfo.setPort(trackerPort);

        NsdManager nsdManager = (NsdManager) getApplicationContext().getSystemService(Context.NSD_SERVICE);

        nsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD, registrationListener);
    }

    private void copyAsset(String asset, String destination) {
        AssetManager assetManager = getAssets();

        try {
            InputStream in = assetManager.open(asset);

            File outFile = new File(destination);
            OutputStream out = new FileOutputStream(outFile);

            Utils.copyFile(in, out);

            in.close();
            out.close();
        } catch (Exception e) {
            Log.d("dots", e.toString());
        }
    }
}
